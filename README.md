# rh-dip-rules
This is Rocket Homes implementation of the QL Rocket Science platform rules engine implementation to expose Drools rules engine to lambda based invoker.  This code also implements DAG processing framework similar to the one in the rh-dip project implemented with python.

## Local Development Machine Setup
### Install rsp-rules-drools-core-1.1.0.jar into local Maven Repo if you are missing the local-repo folder
#### Run this from a command line
```
mvn install:install-file \
	 -DlocalRepositoryPath=local-repo \
	 -Dfile=lib/rsp-rules-drools-core-1.1.0.jar 
	 -DgroupId=com.rsp.drools \
	 -DartifactId=rsp-rules-drools-core \
	 -Dversion=1.1.0 \
	 -Dpackaging=jar \
	 -DgeneratePom=true
   ```
   
## Generating the jar with all dependencies
``` 
mvn clean compile package shade:shade
```
This will generate a new jar under target/
Look for a jar file ending with ihr-rules-lambda-#.#.#.jar
## Running project in Eclipse or similar code editor

The src\test\java\ihr\rule folder contains two classes.  The InvokeRulesRunner.java is a test class to directly call the rules engine and process data.  The InvokeRulesTest.java class is a test class to call the AWS Lambda wrapper that in turn invokes the rules engine.  This simulates code being invoked from AWS Lambda native deployment environment.  Both test classes can be invoked as standard Java applications, nothing else is required to execute them.
