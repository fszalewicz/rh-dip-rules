package com.signavio.droolsexport_eefb20e532ee45dbbece6c13d16c2c89

import java.time.OffsetTime
import java.util.Arrays
import org.slf4j.Logger
import java.text.DecimalFormat
import org.slf4j.LoggerFactory
import com.signavio.dmn.formulae.v3.IndexedListElement
import java.util.ArrayList
import java.util.HashSet
import java.math.BigDecimal
import java.util.List
import java.time.OffsetDateTime
import com.signavio.dmn.formulae.v3.DmnHierarchyFormulae
import java.time.LocalDate
import com.signavio.dmn.formulae.v3.DmnFormulae
import java.util.Collections
import java.math.RoundingMode

global String modelId
global Integer revisionNumber
global List $allMessages
global Logger $logger

declare  DmnFormulae
end

declare  DmnFormulaeLocal  extends  DmnFormulae
end

declare  DmnHierarchyFormulae
end

declare  IndexedListElement
end

declare  AnnotationItem
    decision : String
    annotationName : String
    ruleName : String
    category : AnnotationCategory
    message : String
    timestamp : OffsetDateTime
end

declare  Input
    property : Property
end

declare  ImputeCityBasedOnZip  extends  IndexedListElement
    imputeCityBasedOnZip : String
end

declare  ImputeCityBasedOnZip_Output
    imputeCityBasedOnZip : String
end

declare  ImputeStateBasedOnZip  extends  IndexedListElement
    imputeStateBasedOnZip : String
end

declare  ImputeStateBasedOnZip_Output
    imputeStateBasedOnZip : String
end

declare  ImputePropertyValue  extends  IndexedListElement
    assignedPropertyValue : BigDecimal
    assignedReason : String
end

declare  ImputePropertyValue_Output
    imputePropertyValue : ImputePropertyValue
    assignedPropertyValue : BigDecimal
    assignedReason : String
end

declare  Property
    street : String
    city : String
    state : String
    zip : String
    bedrooms : BigDecimal
    sqfootage : BigDecimal
    id : BigDecimal
    propertyvalue : BigDecimal
end

declare enum AnnotationCategory
	INFO,CRITICAL,REGULATION,POLICY;
end

rule "initialize_version_info"
    no-loop true
    salience 1000
when
then
	drools.getWorkingMemory().setGlobal("modelId", "eefb20e532ee45dbbece6c13d16c2c89");
	drools.getWorkingMemory().setGlobal("revisionNumber", -1);
end

rule "initialize_annotations_list"
    no-loop true
    salience 1000
when
then
	drools.getWorkingMemory().setGlobal("$allMessages", new ArrayList());
end

rule "add_formulae_to_memory"
    no-loop true
    salience 1000
when
then
	insert(new DmnFormulaeLocal());
	insert(new DmnHierarchyFormulae());
end

rule "init_logger"
    no-loop true
    salience 1000
when
then
	drools.getWorkingMemory().setGlobal("$logger", LoggerFactory.getLogger("DroolsExecution"));
end

rule "log_input_rule"
    no-loop true
    salience 999
when
    $input : Input(  )
then
	$logger.info($input.toString());
end

rule "imputeCityBasedOnZip_rule_1"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("480"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("ROYAL OAK");
	$imputeCityBasedOnZip.setIndex(0);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_2"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("481"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("DETROIT");
	$imputeCityBasedOnZip.setIndex(1);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_3"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("482"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("DETROIT");
	$imputeCityBasedOnZip.setIndex(2);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_4"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("483"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("ROYAL OAK");
	$imputeCityBasedOnZip.setIndex(3);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_5"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("484"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("FLINT");
	$imputeCityBasedOnZip.setIndex(4);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_6"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("485"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("FLINT");
	$imputeCityBasedOnZip.setIndex(5);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_7"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("486"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("SAGINAW");
	$imputeCityBasedOnZip.setIndex(6);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_8"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("487"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("SAGINAW");
	$imputeCityBasedOnZip.setIndex(7);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_9"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("488"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("LANSING");
	$imputeCityBasedOnZip.setIndex(8);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_10"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("489"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("LANSING");
	$imputeCityBasedOnZip.setIndex(9);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_11"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("490"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("KALAMAZOO");
	$imputeCityBasedOnZip.setIndex(10);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_12"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("491"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("KALAMAZOO");
	$imputeCityBasedOnZip.setIndex(11);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_13"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("492"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("JACKSON");
	$imputeCityBasedOnZip.setIndex(12);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_14"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("493"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("GRAND RAPIDS");
	$imputeCityBasedOnZip.setIndex(13);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_15"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("497"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("GAYLORD");
	$imputeCityBasedOnZip.setIndex(14);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_16"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("494"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("GRAND RAPIDS");
	$imputeCityBasedOnZip.setIndex(15);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_17"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("495"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("GRAND RAPIDS");
	$imputeCityBasedOnZip.setIndex(16);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_18"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("496"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("TRAVERSE CITY");
	$imputeCityBasedOnZip.setIndex(17);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_19"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("498"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("IRON MOUNTAIN");
	$imputeCityBasedOnZip.setIndex(18);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_rule_20"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    not(
    ImputeCityBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("499"))) )
then
	ImputeCityBasedOnZip $imputeCityBasedOnZip = new ImputeCityBasedOnZip();
	$imputeCityBasedOnZip.setImputeCityBasedOnZip("IRON MOUNTAIN");
	$imputeCityBasedOnZip.setIndex(19);
	insert($imputeCityBasedOnZip);
end

rule "imputeCityBasedOnZip_singleResultOutput"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 0
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    $input : Input(  )
    $imputeCityBasedOnZip : ImputeCityBasedOnZip(  )
then
	ImputeCityBasedOnZip_Output $imputeCityBasedOnZip_Output = new ImputeCityBasedOnZip_Output();
	$imputeCityBasedOnZip_Output.setImputeCityBasedOnZip($imputeCityBasedOnZip.getImputeCityBasedOnZip());
	retract($imputeCityBasedOnZip);
	insert($imputeCityBasedOnZip_Output);
end

rule "imputeCityBasedOnZip_emptyResultFallBack"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience -1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeCityBasedOnZip_Output(  )   )
    $input : Input(  )
then
	ImputeCityBasedOnZip_Output $imputeCityBasedOnZip_Output = new ImputeCityBasedOnZip_Output();
	$imputeCityBasedOnZip_Output.setImputeCityBasedOnZip(null);
	insert($imputeCityBasedOnZip_Output);
end

rule "imputeCityBasedOnZip_cleanup_rule_output"
    no-loop true
    //decision:  imputeCityBasedOnZip
    salience 2
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    ImputeCityBasedOnZip_Output(  )
    $imputeCityBasedOnZip : ImputeCityBasedOnZip(  )
then
	retract($imputeCityBasedOnZip);
end

rule "imputeStateBasedOnZip_rule_1"
    no-loop true
    //decision:  imputeStateBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeStateBasedOnZip_Output(  )   )
    not(
    ImputeStateBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("48"))) )
then
	ImputeStateBasedOnZip $imputeStateBasedOnZip = new ImputeStateBasedOnZip();
	$imputeStateBasedOnZip.setImputeStateBasedOnZip("MI");
	$imputeStateBasedOnZip.setIndex(0);
	insert($imputeStateBasedOnZip);
end

rule "imputeStateBasedOnZip_rule_2"
    no-loop true
    //decision:  imputeStateBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeStateBasedOnZip_Output(  )   )
    not(
    ImputeStateBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("49"))) )
then
	ImputeStateBasedOnZip $imputeStateBasedOnZip = new ImputeStateBasedOnZip();
	$imputeStateBasedOnZip.setImputeStateBasedOnZip("MI");
	$imputeStateBasedOnZip.setIndex(1);
	insert($imputeStateBasedOnZip);
end

rule "imputeStateBasedOnZip_rule_3"
    no-loop true
    //decision:  imputeStateBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeStateBasedOnZip_Output(  )   )
    not(
    ImputeStateBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("35"))) )
then
	ImputeStateBasedOnZip $imputeStateBasedOnZip = new ImputeStateBasedOnZip();
	$imputeStateBasedOnZip.setImputeStateBasedOnZip("AL");
	$imputeStateBasedOnZip.setIndex(2);
	insert($imputeStateBasedOnZip);
end

rule "imputeStateBasedOnZip_rule_4"
    no-loop true
    //decision:  imputeStateBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeStateBasedOnZip_Output(  )   )
    not(
    ImputeStateBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("36"))) )
then
	ImputeStateBasedOnZip $imputeStateBasedOnZip = new ImputeStateBasedOnZip();
	$imputeStateBasedOnZip.setImputeStateBasedOnZip("AL");
	$imputeStateBasedOnZip.setIndex(3);
	insert($imputeStateBasedOnZip);
end

rule "imputeStateBasedOnZip_rule_5"
    no-loop true
    //decision:  imputeStateBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeStateBasedOnZip_Output(  )   )
    not(
    ImputeStateBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("10"))) )
then
	ImputeStateBasedOnZip $imputeStateBasedOnZip = new ImputeStateBasedOnZip();
	$imputeStateBasedOnZip.setImputeStateBasedOnZip("NY");
	$imputeStateBasedOnZip.setIndex(4);
	insert($imputeStateBasedOnZip);
end

rule "imputeStateBasedOnZip_rule_6"
    no-loop true
    //decision:  imputeStateBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeStateBasedOnZip_Output(  )   )
    not(
    ImputeStateBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("11"))) )
then
	ImputeStateBasedOnZip $imputeStateBasedOnZip = new ImputeStateBasedOnZip();
	$imputeStateBasedOnZip.setImputeStateBasedOnZip("NY");
	$imputeStateBasedOnZip.setIndex(5);
	insert($imputeStateBasedOnZip);
end

rule "imputeStateBasedOnZip_rule_7"
    no-loop true
    //decision:  imputeStateBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeStateBasedOnZip_Output(  )   )
    not(
    ImputeStateBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("12"))) )
then
	ImputeStateBasedOnZip $imputeStateBasedOnZip = new ImputeStateBasedOnZip();
	$imputeStateBasedOnZip.setImputeStateBasedOnZip("NY");
	$imputeStateBasedOnZip.setIndex(6);
	insert($imputeStateBasedOnZip);
end

rule "imputeStateBasedOnZip_rule_8"
    no-loop true
    //decision:  imputeStateBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeStateBasedOnZip_Output(  )   )
    not(
    ImputeStateBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("13"))) )
then
	ImputeStateBasedOnZip $imputeStateBasedOnZip = new ImputeStateBasedOnZip();
	$imputeStateBasedOnZip.setImputeStateBasedOnZip("NY");
	$imputeStateBasedOnZip.setIndex(7);
	insert($imputeStateBasedOnZip);
end

rule "imputeStateBasedOnZip_rule_9"
    no-loop true
    //decision:  imputeStateBasedOnZip
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeStateBasedOnZip_Output(  )   )
    not(
    ImputeStateBasedOnZip(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval($F.startsWith($F.lower(((String) ($F.resolve($input, new String[] {"property","zip"})))), $F.lower("14"))) )
then
	ImputeStateBasedOnZip $imputeStateBasedOnZip = new ImputeStateBasedOnZip();
	$imputeStateBasedOnZip.setImputeStateBasedOnZip("NY");
	$imputeStateBasedOnZip.setIndex(8);
	insert($imputeStateBasedOnZip);
end

rule "imputeStateBasedOnZip_singleResultOutput"
    no-loop true
    //decision:  imputeStateBasedOnZip
    salience 0
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeStateBasedOnZip_Output(  )   )
    $input : Input(  )
    $imputeStateBasedOnZip : ImputeStateBasedOnZip(  )
then
	ImputeStateBasedOnZip_Output $imputeStateBasedOnZip_Output = new ImputeStateBasedOnZip_Output();
	$imputeStateBasedOnZip_Output.setImputeStateBasedOnZip($imputeStateBasedOnZip.getImputeStateBasedOnZip());
	retract($imputeStateBasedOnZip);
	insert($imputeStateBasedOnZip_Output);
end

rule "imputeStateBasedOnZip_emptyResultFallBack"
    no-loop true
    //decision:  imputeStateBasedOnZip
    salience -1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputeStateBasedOnZip_Output(  )   )
    $input : Input(  )
then
	ImputeStateBasedOnZip_Output $imputeStateBasedOnZip_Output = new ImputeStateBasedOnZip_Output();
	$imputeStateBasedOnZip_Output.setImputeStateBasedOnZip(null);
	insert($imputeStateBasedOnZip_Output);
end

rule "imputeStateBasedOnZip_cleanup_rule_output"
    no-loop true
    //decision:  imputeStateBasedOnZip
    salience 2
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    ImputeStateBasedOnZip_Output(  )
    $imputeStateBasedOnZip : ImputeStateBasedOnZip(  )
then
	retract($imputeStateBasedOnZip);
end

rule "imputePropertyValue_rule_1"
    no-loop true
    //decision:  imputePropertyValue
    salience 1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputePropertyValue_Output(  )   )
    not(
    ImputePropertyValue(  )   )
    $input : Input(  )
    eval( $F.nullSafeEval(((BigDecimal) ($F.resolve($input, new String[] {"property","propertyvalue"}))) == null) )
then
	ImputePropertyValue $imputePropertyValue = new ImputePropertyValue();
	$imputePropertyValue.setAssignedPropertyValue(BigDecimal.valueOf(25000.0));
	$imputePropertyValue.setAssignedReason("Minimum Viable Value");
	$imputePropertyValue.setIndex(0);
	insert($imputePropertyValue);
end

rule "imputePropertyValue_singleResultOutput"
    no-loop true
    //decision:  imputePropertyValue
    salience 0
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputePropertyValue_Output(  )   )
    $input : Input(  )
    $imputePropertyValue : ImputePropertyValue(  )
then
	ImputePropertyValue_Output $imputePropertyValue_Output = new ImputePropertyValue_Output();
	$imputePropertyValue_Output.setAssignedReason($imputePropertyValue.getAssignedReason());
	$imputePropertyValue_Output.setAssignedPropertyValue($imputePropertyValue.getAssignedPropertyValue());
	$imputePropertyValue_Output.setImputePropertyValue($imputePropertyValue);
	retract($imputePropertyValue);
	insert($imputePropertyValue_Output);
end

rule "imputePropertyValue_emptyResultFallBack"
    no-loop true
    //decision:  imputePropertyValue
    salience -1
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    not(
    ImputePropertyValue_Output(  )   )
    $input : Input(  )
then
	ImputePropertyValue_Output $imputePropertyValue_Output = new ImputePropertyValue_Output();
	$imputePropertyValue_Output.setAssignedReason(null);
	$imputePropertyValue_Output.setAssignedPropertyValue(null);
	insert($imputePropertyValue_Output);
end

rule "imputePropertyValue_cleanup_rule_output"
    no-loop true
    //decision:  imputePropertyValue
    salience 2
when
    $F : DmnFormulaeLocal(  )
    $H : DmnHierarchyFormulae(  )
    ImputePropertyValue_Output(  )
    $imputePropertyValue : ImputePropertyValue(  )
then
	retract($imputePropertyValue);
end
