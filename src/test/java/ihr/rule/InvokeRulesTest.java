package ihr.rule;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import com.rsp.drools.*;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import static org.mockito.Mockito.*;

public class InvokeRulesTest {
	
    public static final void main(String[] args) {
        try {
        	
        	// this invokes the rules engine via a lambda invoker, simulating aws based invocation
        	
        	InvokeRules ir = new InvokeRules();
        	Context context = mock(Context.class);
            LambdaLogger logger = mock(LambdaLogger.class);
            doNothing().when(logger).log(anyString());
            doReturn(logger).when(context).getLogger();
        	
        	String payload = "{\"CorrelationId\":\"30b15fd8-8cd7-4e7c-b1be-a24f1c66bbec\",\"standardStatus\":\"Active\",\"listPrice\":\"125,000\",\"propertyType\":\"single family\"}";
        	
        	InputStream is = new ByteArrayInputStream(payload.getBytes());
        	ByteArrayOutputStream os = new ByteArrayOutputStream();
        	
        	ir.handleRequest(is, os, context);
        	
        	System.out.println(os.toString("UTF-8"));
        	
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

}