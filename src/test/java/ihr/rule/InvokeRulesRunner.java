package ihr.rule;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;

import ihr.dag.*;
import com.rsp.drools.*;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import static org.mockito.Mockito.*;

public class InvokeRulesRunner {
	
    public static final void main(String[] args) {
        try {
        	
        	DAGRunner dag = new DAGRunner();
        	
        	HashMap<String,Object> params = new HashMap<String,Object>();
        	params.put("dag_config", "config/applications/mls_dag_config.yaml");
    		params.put("common_properties", "properties/common.yaml");
    		params.put("app_properties", "properties/applications/rules");
    		
    		String payload = "{\"CorrelationId\":\"30b15fd8-8cd7-4e7c-b1be-a24f1c66bbec\",\"standardStatus\":\"Active\",\"listPrice\":\"125,000\",\"propertyType\":\"single family\"}";
        	    		
    		dag.addToStack("incoming_data", payload);
        	dag.invoke(params);       	
        	
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

}