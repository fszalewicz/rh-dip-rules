package ihr.rule;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.rsp.drools.*;

import ihr.dag.DAGRunner;

public class InvokeRules implements RequestStreamHandler {
  public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) {

	LambdaLogger logger = context.getLogger();
	
	StringBuilder stringBuilder = new StringBuilder();
	
	try 
	{			
		String line = null;
		InputStreamReader s = new InputStreamReader(inputStream, Charset.defaultCharset());
		BufferedReader bufferedReader = new BufferedReader(s);
		while ((line = bufferedReader.readLine()) != null) {
			stringBuilder.append(line);
		}
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
 
	try {
		
		DAGRunner dag = new DAGRunner();
    	
    	HashMap<String,Object> params = new HashMap<String,Object>();
    	params.put("dag_config", "config/applications/mls_dag_config.yaml");
		params.put("common_properties", "properties/common.yaml");
		params.put("app_properties", "properties/applications/rules");
		
		dag.addToStack("incoming_data", stringBuilder.toString());
    	dag.invoke(params);   
    	
    	String response = "{\"statusCode\": \"200\", \"body\": \"OK\", \"headers\": { \"Content-Type\": \"application/json\" }}";
    	
    	outputStream.write(response.getBytes(Charset.forName("UTF-8")));
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	    
	    String response = "{\"statusCode\": \"400\", \"body\": \"" + e.getStackTrace() + "\", \"headers\": { \"Content-Type\": \"application/json\" }}";
    	
    	try {
			outputStream.write(response.getBytes(Charset.forName("UTF-8")));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}


  }
}

