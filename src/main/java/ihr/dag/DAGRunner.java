package ihr.dag;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.lang.reflect.Method;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class DAGRunner {
	
	private static HashMap<String, Object> stack = new HashMap<String, Object>();
	private static HashMap<String, Object> dependecies = new HashMap<String, Object>();
	private static LinkedHashMap<String, Object> tasks = new LinkedHashMap<String, Object>();
	private JSONObject dagconfig = null;
	private static DAGLogger logger = new DAGLogger(null);
	private static String env = System.getenv("ENV_NAME") != null ? System.getenv("ENV_NAME") : "local";
	
	public void addToStack(String key, Object o) {

		// check if the output variable against the dag stack
		if(stack.containsKey(key)) {
			stack.replace(key, o);
		} else {
			stack.put(key, o);
		}
		
	}
	
	public void invoke(Map<String,Object> params) {
		
		logger.info("Start");
		
		String dagfilename = params.containsKey("dag_config") ? params.get("dag_config").toString() : "config/applications/dag_config.json"; 
		logger.info("Found dagfilename: {}", dagfilename);
		dagconfig = this.loadFile(dagfilename);
		
		// set runtime env variable if it was passed in
		env = params.containsKey("ENV_NAME") ? params.get("ENV_NAME").toString() : env;
		
		String commonprops = params.containsKey("common_properties") ? params.get("common_properties").toString() : "properties/applications/common.json"; 
		logger.info("Found commonprops: {}", commonprops);
		mergeProperties(dagconfig, commonprops);
		
		String appprops = params.containsKey("app_properties") ? params.get("app_properties").toString() : "properties/applications"; 
		logger.info("Found appprops: {}", appprops);
		mergeProperties(dagconfig, appprops);
		
		loadDependecies();
		loadTasks();

		// run
		for(Map.Entry<String, Object> task : tasks.entrySet()) {
			String methodName = ((DAGTask)task.getValue()).getConfig().getString("run_method");
			try {
				
				// invoke the main method if specified
				Method method = task.getValue().getClass().getMethod(methodName);
				if(method != null) {
					
					DAGTask tsk = ((DAGTask)task.getValue());
					// initialize the input stack for the task
					HashMap<String, Object> _inputstack = new HashMap<String, Object>();
					if(tsk.getConfig().has("inputs")) {
						JSONArray _inputs = tsk.getConfig().getJSONArray("inputs");
						Iterator iter = _inputs.iterator();
						while(iter.hasNext()) {
							String key = iter.next().toString();
							// first make sure to only process variables that were asked for from the function
							// if defined in the config inputs section
							if(stack.containsKey(key)) {
								_inputstack.put(key, stack.get(key));
							}
						}
						tsk.setInput(_inputstack);
					}
									
					// invoke the main process method
					method.invoke(tsk);

					// collect from the output stack and add it to the common stack
					HashMap<String, Object> _outstack = tsk.getOutputs();
					if(tsk.getConfig().has("outputs")) {						
						JSONArray _outputs = tsk.getConfig().getJSONArray("outputs");
						Iterator iter = _outputs.iterator();
						while(iter.hasNext()) {
							String key = iter.next().toString();
							// first make sure to only process variables that were returned from the function
							// if defined in the config outputs section
							if(_outstack.containsKey(key)) {
								this.addToStack(key, _outstack.get(key));
							}
						}
					}
				}
				
			} catch (Exception e) {
				logger.error("Could not find class method: {}", e.getStackTrace());
			}
		}
		
		logger.info("Finish");
	}
	
	private void loadTasks() {
	
		JSONArray tasklist = dagconfig.getJSONArray("tasks");
		
		if(tasklist != null) {
			for (int idx = 0; idx < tasklist.length(); idx++) {
			  JSONObject o = tasklist.getJSONObject(idx);
				Object key = o.keys().next();				
				JSONObject task = o.getJSONObject(key.toString());
				tasks.put(key.toString(), createInstance(task));
			}
		}
	}
	
	private void loadDependecies() {
		
		JSONArray deps = dagconfig.getJSONArray("bootstrap");
		
		if(deps != null) {
			for (int idx = 0; idx < deps.length(); idx++) {
			  JSONObject o = deps.getJSONObject(idx);
				Object key = o.keys().next();				
				JSONObject dependency = o.getJSONObject(key.toString());
				dependecies.put(key.toString(), createInstance(dependency));
			}
		}
	}
	
	private Object createInstance(JSONObject jobj) {
		Object retobj = null;
		
		try {

			if(jobj.has("dependencies") || jobj.has("inputs")) {
				HashMap<String, Object> deps = new HashMap<String, Object>();

				// if dependencies are specified, must only get the dependencies asked for
				if(jobj.has("dependencies")) {
					JSONArray jdep = jobj.getJSONArray("dependencies");
					Iterator iter = jdep.iterator();
					while(iter.hasNext()) {
						String key = iter.next().toString();
						if(dependecies.containsKey(key)) {
							deps.put(key,  dependecies.get(key));
						}
					}
				}
				
				// if inputs are specified, must only get the inputs asked for
				if(jobj.has("inputs")) {
					JSONArray jdep = jobj.getJSONArray("inputs");
					Iterator iter = jdep.iterator();
					while(iter.hasNext()) {
						String key = iter.next().toString();
						if(stack.containsKey(key)) {
							deps.put(key, stack.get(key));
						}
					}				
				}
				
				Constructor cls = Class.forName(jobj.getString("impl")).getConstructor(JSONObject.class, HashMap.class);
				retobj = cls.newInstance(jobj, deps);
				
			} else {
				Constructor cls = Class.forName(jobj.getString("impl")).getConstructor(JSONObject.class);
				retobj = cls.newInstance(jobj);
			}
			
		} catch (ClassNotFoundException e) {
			logger.error("Class implementation not found: {}\n{}", jobj.getString("impl"), e.getStackTrace());
		} catch (Exception e) {
			logger.error("Could not create class: {}", e.getStackTrace());
		}
		
		return retobj;
	}
	
	private void mergeProperties(JSONObject doc, String filename) {
		Path file = new File(filename).toPath();
		if(Files.isDirectory(file)) {
			try {
				Stream<Path> walk = Files.walk(file);
				Iterator iter = walk.iterator();
				while(iter.hasNext()) {
					Path curfile = (Path)iter.next();
					if(curfile.toFile().isFile() && curfile.toFile().getName().toLowerCase().contains(env.toLowerCase())) {
						JSONObject props = this.loadFile(curfile.toFile().getAbsolutePath());
						inspectJSON(doc, props);
					}
				}				
			} catch (IOException e) {
				logger.error("Could not walk path for : {}\n{}", filename, e.getStackTrace());
			}
		}else {
			JSONObject props = this.loadFile(file.toFile().getAbsolutePath());
			inspectJSON(doc, props);			
		}
	}
	
	private void inspectJSON(JSONObject o, JSONObject props) {
		Iterator<String> keys = o.keys();

		while(keys.hasNext()) {
		    String key = keys.next();
		    Object c = o.get(key);
		    if (c instanceof JSONObject) {
		          // logger.info("key and val:{}, {}", key, c.toString());   
		          inspectJSON((JSONObject)c, props);
		          
		    } else if (c instanceof JSONArray) {
		    	
		    	JSONArray arr = (JSONArray)c;
		    	for (int idx = 0; idx < arr.length(); idx++) {
		    		if(arr.get(idx) instanceof JSONObject) {
		    			inspectJSON(arr.getJSONObject(idx), props);
		    		}
				}
		    	
		    } else {
		    	// if the value of the property is a pointer to a file
		    	// load the file contents first before trying to resolve
		    	// properties
		    	if(c.toString().toLowerCase().matches("(.*).yaml|(.*).json")) {
		    		o.put(key, loadFile(c.toString()));
		    		c = o.get(key);
		    		inspectJSON((JSONObject)c, props);
		    	}

		    	// look to see if a property value is to be replaced and
		    	// get it from properties first, if not there check env
		    	// variables
		    	if(c.toString().toLowerCase().matches("\\$\\{(.*)\\}")) {
		    		String propkey = c.toString().substring(2, c.toString().length() - 1);
		    		String resolvedprop = this.getJSONObjectValue(propkey, props);
		    		if(resolvedprop.isEmpty()) {
		    			// one more try to check env variables
		    			resolvedprop = System.getenv(propkey) != null ? System.getenv(propkey) : resolvedprop;
		    		}
		    		o.put(key, !resolvedprop.isEmpty() ? resolvedprop : c.toString());
		    		//logger.info("key and val prop:{}, {}, {}", key, c.toString().substring(2, c.toString().length() - 1), resolvedprop);

		    	}
		    }
	    }
	}
		
	private String getJSONObjectValue(String complexKey, JSONObject o) {
	
		String ret = "";
		Object obj = o.query("/" + complexKey.replace(".", "/"));
		if (obj != null) {
			ret = obj.toString();
		}		
		return ret;
	}
	
	private JSONObject loadFile(String filename) {
		JSONObject jo = null;
		
		if(filename == null)
			return jo;
			
		try {
			
			String content = new String(Files.readAllBytes(Paths.get(filename)));
			if(content.isEmpty()) {
				// get the file contents as a resource stream, I dare you 
				// to beat the speed of this, yes Filip put this in to see
				// if anyone will read these comments
				InputStream inputStream = getClass().getResourceAsStream(filename);
				ByteArrayOutputStream result = new ByteArrayOutputStream();
				byte[] buffer = new byte[1024];
				int length;
				while ((length = inputStream.read(buffer)) != -1) {
				    result.write(buffer, 0, length);
				}
				content = result.toString("UTF-8");
			} 
			if(!content.isEmpty()) {				
				jo = filename.toLowerCase().endsWith(".yaml") ? this.convertToJson(content) : new JSONObject(content);
			}
				
		} catch(Exception e) {
			logger.error("Could not load file: {}\n{}", filename, e.getStackTrace());
		}
		
		return jo;
	}
	
	private JSONObject convertToJson(String yamlString) {
	    Yaml yaml= new Yaml();
	    Map<String,Object> map= (Map<String, Object>) yaml.load(yamlString);

	    return new JSONObject(map);
	}
}