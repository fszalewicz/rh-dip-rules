package ihr.dag;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DAGLogger {

	private Logger logger = null;
	
	public DAGLogger(JSONObject config) {
		
		JSONObject params = (config != null && config.has("params") ? (JSONObject) config.get("params") : null);
		
		String logname = params != null && params.has("app_name") ? params.getString("app_name") : "ihr_dag_logger";
		logger = LoggerFactory.getLogger(logname);
	}
	
	public Logger getBaseLogger( ) {
		return this.logger;
	}
	
	public void info(String msg) {
		this.logger.info(msg);
	}
	
	public void info(String msg, Object... arguments) {
		this.logger.info(msg, arguments);
	}
	
	public void error(String msg, Object... arguments) {
		this.logger.error(msg, arguments);
	}
}