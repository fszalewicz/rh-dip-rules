package ihr.dag;

import java.util.HashMap;
import org.json.JSONObject;

public abstract class DAGTask {
	
	private HashMap<String, Object> _inputstack = new HashMap<String, Object>();
	private HashMap<String, Object> _outputstack = new HashMap<String, Object>();
	private HashMap<String, Object> _dependencies = null;
	private DAGLogger _logger = null;
	private JSONObject _config = null;

	public JSONObject getConfig() {
		return _config;
	}

	public JSONObject getConfig(String key) {
		if(key != null && _config.has(key)) {
			return _config.getJSONObject(key);
		}
		return _config;
	}

	public DAGLogger getLogger() {
		return _logger;
	}

	public void init(JSONObject config) {
		
		this.init(config, null);
	
	}

	public void init(JSONObject config, HashMap<String, Object> dependencies) {
		
		_config = config;
		
		if(dependencies != null) {
			_dependencies = (HashMap<String, Object>)dependencies.clone();
			
			if(dependencies != null && dependencies.containsKey("logger")) {
				_logger = (DAGLogger)dependencies.get("logger");
			}
		}
	
	}
	
	public void setInput(HashMap<String, Object> inputs) {
		_inputstack = (HashMap<String, Object>) inputs.clone();
	}
	
	public HashMap<String, Object> getOutputs() {
		return _outputstack;
	}
	
	public Object getInput(String key) {
		if(_inputstack.containsKey(key))
			return _inputstack.get(key);
		else
			return null;
	}
	
	public void setOutput(String key, Object value) {
		if(_outputstack.containsKey(key))
			_outputstack.replace(key, value);
		else
			_outputstack.put(key, value);
	}
	
}