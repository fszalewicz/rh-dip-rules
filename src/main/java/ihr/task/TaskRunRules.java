package ihr.task;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import ihr.dag.DAGTask;
import ihr.dag.DAGLogger;
import com.rsp.drools.*;

public class TaskRunRules extends DAGTask {

	private static DAGLogger logger = null;
	private String _parobject = "";
	private String _parpackage = "";
	private String _parwrapper = "";
	private String _parruleset = "";
	private String _pardataset = "";
	private JSONArray _parmapper = null;
	
	public TaskRunRules(JSONObject config, HashMap<String, Object> dependencies) {
		
		this.init(config, dependencies);
		
		this.getLogger().info("inside of taskrunrules init code");
		this._parobject = config.query("/params/rules/object").toString();
		this._parpackage = config.query("/params/rules/package").toString();
		this._parwrapper = config.query("/params/rules/wrapper").toString();
		this._parruleset = config.query("/params/rules/ruleset").toString();
		this._pardataset = config.query("/params/rules/data").toString();
		this._parmapper = (JSONArray)config.query("/params/rules/mapping");
		
	}

	public void process() {
				
		this.getLogger().info("process method of taskrunrules is invoked");
		
		HashMap<String,String> structFields = new HashMap<String,String>();
		structFields.put("ruleconfiguration", "{\"Object\":\"" + this._parobject + "\","
				+ "\"Package\":\"" + this._parpackage + "\","
				+ "\"Wrapper\":\"" + this._parwrapper + "\","
				+ "\"CorrelationId\":\"123-0\","
				+ "\"BusinessProcessId\":\"123-1\","
				+ "\"BusinessActivityId\":\"123-2\","
				+ "\"Ruleset\":\"" + this._parruleset + "\"}");

		// {"standardStatus":"Active","listPrice":"125,000","propertyType":"single family"}
		JSONObject input = (JSONObject) this.getInput("data_loaded");
		
		JSONObject data = (JSONObject)this.getInput(this._pardataset);
		
		if(this._parmapper != null && input != null) {
			for (int idx = 0; idx < this._parmapper.length(); idx++) {
			  JSONObject o = this._parmapper.getJSONObject(idx);
			  // if the input data has a physical column, label it with logical name
			  // and add it to the stack being sent to the rules engine for processing
			  if(input.has(o.getString("physical"))) {
				  structFields.put(o.getString("logical"), input.getString(o.getString("physical")));
			  }
			}
		}
        
		Map<String,String> ret = new HashMap<String,String>();       	
		
		RunRules rr = new RunRules();
		rr.setLogger(this.getLogger().getBaseLogger());
		
		try {
			ret = rr.call(structFields);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (Map.Entry<String, String> kv : ret.entrySet()) {
			this.getLogger().info("RET VAL: " + kv.getKey() + " - " + kv.getValue());
		} 
		
	}
}