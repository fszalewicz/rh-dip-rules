package ihr.task;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import ihr.dag.DAGLogger;
import ihr.dag.DAGTask;
import com.rsp.drools.*;

public class TaskProcessData extends DAGTask {
		
	private String _parinputdata = "";
		
	public TaskProcessData(JSONObject config, HashMap<String, Object> dependencies) {
		
		this.init(config, dependencies);
		
		this._parinputdata = ((JSONArray)config.query("/inputs")).getString(0);
		this.getLogger().info("inside of taskprocessdata init code");

	}

	public void process() {
		
		this.getLogger().info("process method of taskprocessdata is invoked");
		
		//String value = "{\"standardStatus\":\"Active\",\"listPrice\":\"125,000\",\"propertyType\":\"single family\"}";
		String value = (String) this.getInput(this._parinputdata);
		
		JSONObject json = null;
		try {
			json =  new JSONObject(value);
		} catch (Exception e) {
			this.getLogger().error("Could not convert data to JSON.", e.getStackTrace());
		}
		
		JSONArray oarr = this.getConfig().getJSONArray("outputs");
		this.setOutput(oarr.get(0).toString(), json);
		
	}
}